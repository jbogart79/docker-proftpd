FROM ubuntu:latest

RUN apt-get update -y &&\
    apt-get install -y proftpd

EXPOSE 21
EXPOSE 2222

ADD ./etc/proftpd/ /etc/proftpd/

CMD ["proftpd", "-n", "-c", "/etc/proftpd/proftpd.conf"]